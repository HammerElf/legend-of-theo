﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Animator animator;

    Vector3 moveDir = Vector3.zero;

    public float moveSpeed = 10;
    public float rotateSpeed = 10;
    float gravity = 10;
    float rot = 0f;
    public bool secretWallClose;

    //My variables
    public bool isAnimated;
    public bool isFalling;
    public Transform cam;
    public bool grounded = true;
    public bool isWalking;
    public float jumpForce = 10f;

    Rigidbody rb;

	// Use this for initialization
	void Start ()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Movement();
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Attack();
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
	}

    private void FixedUpdate()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Walkable")
        {
            grounded = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.gameObject.tag == "Walkable")
        {
            grounded = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Collectible")
        {
            Debug.Log("Collected");
            other.gameObject.SetActive(false);
        }
        else if(other.gameObject.tag == "WinTrigger")
        {
            Debug.Log("You Win!!!");
        }
        else if(other.gameObject.tag == "SecretWall")
        {
            secretWallClose = true;
        }
        if(other.gameObject.tag == "Walkable")
        {
            grounded = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "SecretWall")
        {
            secretWallClose = false;
        }
        else if(other.gameObject.tag == "Walkable")
        {
            grounded = false;
        }
    }

    void Attack()
    {
        animator.Play("Unarmed-Attack-R1");
    }

    void Jump()
    {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }

    void Movement()
    {
        if (grounded)
        {
            if (isFalling) moveDir.y = -gravity / Time.deltaTime;
            isFalling = false;
            animator.SetBool("Falling", false);

            //Move forward
            if (Input.GetKeyDown(KeyCode.W))
            {
                isWalking = true;
                animator.SetBool("Moving", true);
            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                isWalking = false;
                Debug.Log("key up w");
                animator.SetBool("Moving", false);
                if(animator.GetBool("Running"))
                {
                    animator.SetBool("Running", false);
                }
            }

            if(Input.GetKeyDown(KeyCode.LeftShift) && isWalking)
            {
                Debug.Log("Start moving animation");
                animator.SetBool("Running", true);
                //animator.SetBool("Moving", false);
            }
            else if(Input.GetKeyUp(KeyCode.LeftShift))
            {
                Debug.Log("key up left shift");
                animator.SetBool("Running", false);
                //if(Input.GetKey(KeyCode.W) && !animator.GetBool("Moving")) { animator.SetBool("Moving", true); }
                //animator.SetBool("Moving", true);
            }

            if(animator.GetBool("Running"))
            {
                transform.Translate(Vector3.forward * moveSpeed * 2 * Time.deltaTime);
            }
            else if(isWalking)
            {
                transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
            }

            //Move left
            if(Input.GetKey(KeyCode.A))
            {
                animator.SetBool("StrafeLeft", true);
                transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
            }
            else if(Input.GetKeyUp(KeyCode.A))
            {
                animator.SetBool("StrafeLeft", false);
            }

            //Move right
            if(Input.GetKey(KeyCode.D))
            {
                animator.SetBool("StrafeRight", true);
                transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
            }
            else if(Input.GetKeyUp(KeyCode.D))
            {
                animator.SetBool("StrafeRight", false);
            }

            //Move back
            if(Input.GetKey(KeyCode.S))
            {
                animator.SetBool("StrafeBack", true);
                transform.Translate(Vector3.back * moveSpeed * Time.deltaTime);
            }
            else if(Input.GetKeyUp(KeyCode.S))
            {
                animator.SetBool("StrafeBack", false);
            }
        }
        else
        {
            animator.SetBool("Falling", true);
            isFalling = true;
        }

        //if (Input.GetAxis("Horizontal") != 0)
        //{
        //if (Input.GetAxis("Horizontal") > 0) animator.SetBool("RotateRight", true);
        //else if (Input.GetAxis("Horizontal") < 0) animator.SetBool("RotateLeft", true);

        float h = rotateSpeed * Input.GetAxis("Mouse X");
        float v = rotateSpeed * Input.GetAxis("Mouse Y");

        transform.Rotate(Vector3.up, h, Space.Self);
        cam.Rotate(Vector3.right, -v, Space.Self);
        //}
        //else if(Input.GetAxis("Horizontal") == 0)
        //{
        //    animator.SetBool("RotateRight", false);
        //    animator.SetBool("RotateLeft", false);
        //}
    }
}
